package grdj.com.ifix;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import grdj.com.ifix.adapter.MainAdapter;
import grdj.com.ifix.globals.GlovalVars;
import grdj.com.ifix.model.Contact;
import grdj.com.ifix.rest.RestContactList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Contact> data= new ArrayList<>();
    private MainAdapter adapter;
    private Context _context;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    public String get_network()	{
        String network_type="UNKNOWN";//maybe usb reverse tethering
        NetworkInfo active_network=((ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (active_network!=null && active_network.isConnectedOrConnecting()) {
            if (active_network.getType()==ConnectivityManager.TYPE_WIFI) {
                network_type="WIFI";
            }else if (active_network.getType()==ConnectivityManager.TYPE_MOBILE) {
                network_type=((ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo().getSubtypeName();
            }
        }
        return network_type;
    }

    private void initViews(){
        recyclerView = (RecyclerView)findViewById(R.id.mi_recicler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        spinner = (ProgressBar)this.findViewById(R.id.progressBarMain);
        spinner.setVisibility(View.VISIBLE);
        _context=this;
        if(get_network()!="UNKNOWN"){
            loadJSON();
        }else{

            spinner.setVisibility(View.GONE);

            new MaterialDialog.Builder(this)
            .title("Sin conexión")
            .content("No encontramos ninguna conexión a Internet")
            .positiveText("Aceptar")
            .negativeText("Cancelar")
            .onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    // TODO
                }
            })
            .onNeutral(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    // TODO
                }
            })
            .onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    // TODO
                }
            }).show();
        }
    }

    private void loadJSON(){
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://private-d0cc1-iguanafixtest.apiary-mock.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestContactList restClient = retrofit.create(RestContactList.class);
        Call<List<Contact>> call = restClient.getData();

        call.enqueue(new Callback<List<Contact>>() {

            @Override
            public void onResponse(Call<List<Contact>> call, Response<List<Contact>> response) {
                switch (response.code()) {
                    case 200:
                        data = response.body();
                        Log.d("DATA","DATA: "+data.get(0).getFirstName());
                        adapter= new MainAdapter(_context, data);
                        adapter.setOnRowClickedListener(new MainAdapter.OnRowClickedListener() {
                            @Override
                            public void OnRowClickedClicked(String id) {
                                //Log.d("ID: ",id);
                                GlovalVars.setUserId(id);
                                Intent it = new Intent(MainActivity.this,ContactDataActivity.class);
                                startActivity(it);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        spinner.setVisibility(View.GONE);
                        break;
                    case 401:
                        Log.d("DATA","DATA: 401");
                        break;
                    default:
                        Log.d("DATA","DATA: default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }
}