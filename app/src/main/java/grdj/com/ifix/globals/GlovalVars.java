package grdj.com.ifix.globals;

/**
 * Created by Cgcman on 10/05/2017.
 */
public class GlovalVars {
    private static GlovalVars ourInstance = new GlovalVars();
    private static String USER_ID;

    public static GlovalVars getInstance() {

        return ourInstance;
    }

    public static void setUserId(String id){
        USER_ID=id;
    }

    public static String getUserId(){
        return USER_ID;
    }

    private GlovalVars() {
    }
}
