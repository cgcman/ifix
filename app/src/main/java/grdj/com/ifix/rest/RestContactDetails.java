package grdj.com.ifix.rest;

import grdj.com.ifix.model.Contact;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by gceballos on 5/9/2017.
 */

public interface RestContactDetails {
    @GET("contacts/{id}")
    Call<Contact> getContact(@Path("id") String id);
}