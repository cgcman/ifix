package grdj.com.ifix.rest;

import java.util.List;

import grdj.com.ifix.model.Contact;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Cgcman on 08/05/2017.
 */

public interface RestContactList {
    @GET("contacts")
    Call<List<Contact>> getData();
}