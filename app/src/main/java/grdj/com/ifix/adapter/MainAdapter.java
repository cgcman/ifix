package grdj.com.ifix.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import grdj.com.ifix.MainActivity;
import grdj.com.ifix.R;
import grdj.com.ifix.model.Contact;

import static grdj.com.ifix.adapter.MainAdapter.ContactViewHolder.name;

/**
 * Created by Cgcman on 08/05/2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ContactViewHolder> {
    private List<Contact> list;
    private Context _context;


    public MainAdapter(Context context,List<Contact> contactList) {
        list = contactList;
        _context = context;
    }

    public void swap(List<Contact> newList) {
        list.clear();
        list.addAll(newList);
        notifyDataSetChanged();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MainActivity act= new MainActivity();
        return new ContactViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        final Contact contact = list.get(position);
        Log.d("Nombre","Nombre:"+contact.getFirstName() );
        name.setText(contact.getFirstName()+" "+contact.getLastName());
        if(contact.getPhones().get(1).getNumber()==null){
            holder.phone.setText("");
        }else{
            holder.phone.setText("Phone: "+contact.getPhones().get(1).getNumber());
        }
        Glide.with(_context).load(contact.getThumb()).into(ContactViewHolder.personPhoto);


        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RowClickedListener != null) {
                    final String id = contact.getUserId();
                    RowClickedListener.OnRowClickedClicked(id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        static TextView name;
        static TextView phone;
        static ImageView personPhoto;
        static CardView cv;

        public ContactViewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.person_name);
            phone = (TextView)itemView.findViewById(R.id.person_phone);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
            cv= (CardView)itemView.findViewById(R.id.cvRow);
        }
    }

    public interface OnRowClickedListener {
        void OnRowClickedClicked(String id);
    }

    private OnRowClickedListener RowClickedListener;

    public void setOnRowClickedListener(OnRowClickedListener l) {
        RowClickedListener = l;
    }
}
