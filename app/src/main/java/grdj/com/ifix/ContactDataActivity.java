package grdj.com.ifix;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import grdj.com.ifix.globals.GlovalVars;
import grdj.com.ifix.model.Contact;
import grdj.com.ifix.rest.RestContactDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Cgcman on 10/05/2017.
 */

public class ContactDataActivity extends AppCompatActivity {


    private Context _context;
    private RestContactDetails restContactDetails;
    private ImageView personPhoto;
    private TextView name;
    private TextView data;
    private TextView contact_phones;
    private ProgressBar spinner;
    private CardView cvData;
    private CardView cv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_data_layout);
        initViews();
    }

    private void initViews(){
        _context=this;
        personPhoto=(ImageView)this.findViewById(R.id.contact_image);
        name=(TextView)this.findViewById(R.id.person_full_name);
        data=(TextView)this.findViewById(R.id.person_data);
        contact_phones=(TextView)this.findViewById(R.id.contact_phones);
        spinner = (ProgressBar)this.findViewById(R.id.progressBar);
        spinner.setVisibility(View.VISIBLE);
        cv=(CardView)this.findViewById(R.id.cv);
        cvData=(CardView)this.findViewById(R.id.cv);
        cv.setVisibility(View.GONE);
        cvData.setVisibility(View.GONE);
        loadJSON();
    }

    private void loadJSON(){
        Gson gson = new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://private-d0cc1-iguanafixtest.apiary-mock.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        restContactDetails = retrofit.create(RestContactDetails.class);
        Call<Contact> call = restContactDetails.getContact(GlovalVars.getUserId());

        call.enqueue(new Callback<Contact>() {

            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                switch (response.code()) {
                    case 200:
                        //Log.d("DATA","DATA: "+response.body().getFirstName());
                        Glide.with(_context).load(response.body().getPhoto()).into(personPhoto);
                        name.setText(response.body().getFirstName()+" "+response.body().getLastName());

                        if(response.body().getAddresses().get(0).getWork()==null|| response.body().getAddresses().get(0).getWork()!=""){
                            data.setText("");
                        }else{
                            data.setText(Html.fromHtml((String) "<b>Address:</b> "+response.body().getAddresses().get(0).getWork()));
                        }

                        String _phone1;
                        String _phone2;
                        String _phone3;

                        if(response.body().getPhones().get(0).getNumber()!=null || response.body().getPhones().get(0).getNumber()!="" || response.body().getPhones().get(0).getNumber()!="null"){
                            _phone1= (String) response.body().getPhones().get(0).getNumber();
                        }else{
                            _phone1="";
                        }

                        if(response.body().getPhones().get(1).getNumber()!=null || response.body().getPhones().get(1).getNumber()!="" || response.body().getPhones().get(1).getNumber()!="null"){
                            _phone2="<br>"+response.body().getPhones().get(1).getNumber();
                        }else{
                            _phone2="";
                        }

                        if(response.body().getPhones().get(2).getNumber()!=null || response.body().getPhones().get(2).getNumber()!="" || response.body().getPhones().get(2).getNumber()!="null"){
                            _phone3="<br>"+response.body().getPhones().get(2).getNumber();
                        }else{
                            _phone3="";
                        }

                        contact_phones.setText(Html.fromHtml((String) "<b>Phone Numbers:</b><br>"+_phone1+_phone2+_phone3));
                        cv.setVisibility(View.VISIBLE);
                        cvData.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.GONE);
                        break;
                    case 401:
                        Log.d("DATA","DATA: 401");
                        break;
                    default:
                        Log.d("DATA","DATA: default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });


    }
}
